# -*- coding: latin-1 -*-

import os
import sys

BASE = os.path.dirname(os.path.abspath(__file__))
site_packages = os.path.join(BASE, 'libs')
sys.path.append(site_packages)

import re
import requests
import hashlib
import lxml.html
import xlwt
import glob


headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36',
    'Accept': 'image/webp,image/*,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate, sdch',
    'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4',
    # 'Referer': url,
    "Host": "simba.isr.umich.edu",
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
}
req = requests.Session()
req.headers = headers


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


class Scraper(object):
    cache = False
    pount = {"x": 0, "y": 0}

    def get_variables(me, pdf):
        test_str = ""
        with open(pdf, "rb") as f:
            test_str = f.read().decode("ISO-8859-1", "ignore")
            regex = r"\/Title\s+\(([a-zA-Z]{1,9}[0-9]{1,10})\s+"
            matches = re.finditer(regex, test_str, re.IGNORECASE)
            return sorted(list(set([match.group(1) for matchNum, match in enumerate(matches)])))

    def get_year(me, filename):
        regex = r"([0-9]{2,4})"
        matches = re.finditer(regex, filename, re.IGNORECASE)
        for matchNum, match in enumerate(matches):
            return match.group(1)
        return -2000

    def run(me):
        print("Begin")
        for pdf in glob.glob("pdf/*.pdf"):
            head, filename = os.path.split(pdf)
            fileoutput = "data/xls/%s.xls" % filename.replace(".pdf", "")
            if os.path.exists(fileoutput):
                continue
            print("parser to %s file" % filename)
            # me.get("http://simba.isr.umich.edu/VS/s.aspx")
            me.book_single = xlwt.Workbook()
            me.do_file = MemoryDoFile()
            me.do_file.year_current = me.get_year(filename)
            me.pount["y"] = 0
            variables = me.get_variables(pdf)

            for index, group in enumerate(chunks(variables, 10000)):
                me.sheet = me.book_single.add_sheet("group %d" % (index + 1))
                for variable in group:
                    try:
                        print(variable)
                        me.parse_to_xls(variable)
                    except Exception as e:
                        # print("Err", e)
                        # raise e
                        continue
            # continue

            # me.book.save("data/xls/%s.xls" % "by_sheet")
            me.book_single.save(fileoutput)
            me.do_file.save("data/do/%s.do" % filename.replace(".pdf", ""))
            print("outputs in %s" % fileoutput)
            # break
        print("End")

    def parse_to_xls(me, variable):
        uri = "http://simba.isr.umich.edu/cb.aspx?vList=%s" % variable
        response = me.get(uri)
        # me.parse_to_xls_sheet(variable, response)
        me.parse_to_xls_single_sheet(variable, response)

    def parse_to_xls_sheet(me, variable, html_body):
        sheet = me.book.add_sheet(variable)
        page = lxml.html.document_fromstring(html_body)
        try:
            table = page.xpath("//table//table[1]")[0]
        except Exception as e:
            return

        title = table.xpath("tr[1]/td[2]").pop().text.strip()
        row_pount = 0
        row = sheet.row(row_pount)
        row.write(0, title)
        row_pount += 1

        # contenido
        for x in range(2, 7):
            search_years_available = table.xpath("tr[2]/td/table/tr[%d]/td/table/tr/td[1]" % x)
            for y in search_years_available:
                if "YearsAvailable:" in y.text_content().strip():
                    years_available = table.xpath("tr[2]/td/table/tr[%d]/td/table/tr/td[2]/text()" % x).pop().strip()
                    row = sheet.row(row_pount)
                    row.write(0, years_available)
                    row_pount += 1

        comments = table.xpath("tr[2]/td/table/tr[1]/td/text()")
        comments += table.xpath("tr[2]/td/table/tr[2]/td/text()")
        for txt in comments:
            if txt.strip():
                row = sheet.row(row_pount)
                row.write(0, txt.strip())
                row_pount += 1

        # Value/Range    Text
        for i in range(0, 5):
            search_value_range = table.xpath("tr[2]/td/table/tr[%d]/td/table/tr/td[3]" % i)
            if search_value_range:
                if "Value/Range" in search_value_range[0].text_content():
                    value_range = table.xpath("tr[2]/td/table/tr[%d]/td/table/tr" % (i))
                    # print(i)
                    for index in range(1, len(value_range)):
                        row = sheet.row(row_pount)
                        row.write(0, value_range[index].xpath("td[3]/text()").pop().strip())
                        row.write(1, value_range[index].xpath("td[4]/text()").pop().strip())
                        row_pount += 1

    def parse_to_xls_single_sheet(me, variable, html_body):
        do_body = ""
        sheet = me.sheet
        page = lxml.html.document_fromstring(html_body)
        try:
            table = page.xpath("//table//table[1]")[0]
        except Exception as e:
            return
        row_pount = me.pount["y"]

        row = sheet.row(row_pount)
        row.write(0, "Variable:")
        row.write(1, variable)
        row_pount += 1

        title = table.xpath("tr[1]/td[2]").pop().text.strip()
        row = sheet.row(row_pount)
        row.write(0, title)
        row_pount += 1

        # contenido
        for x in range(2, 7):
            search_years_available = table.xpath("tr[2]/td/table/tr[%d]/td/table/tr/td[1]" % x)
            for y in search_years_available:
                if "YearsAvailable:" in y.text_content().strip():
                    years_available = table.xpath("tr[2]/td/table/tr[%d]/td/table/tr/td[2]/text()" % x).pop().strip()
                    do_body = years_available
                    row = sheet.row(row_pount)
                    row.write(0, years_available)
                    row_pount += 1

        comments = table.xpath("tr[2]/td/table/tr[1]/td/text()")
        comments += table.xpath("tr[2]/td/table/tr[2]/td/text()")
        for txt in comments:
            if txt.strip():
                row = sheet.row(row_pount)
                row.write(0, txt.strip())
                row_pount += 1

        # Value/Range    Text
        for i in range(0, 5):
            search_value_range = table.xpath("tr[2]/td/table/tr[%d]/td/table/tr/td[3]" % i)
            if search_value_range:
                if "Value/Range" in search_value_range[0].text_content():
                    value_range = table.xpath("tr[2]/td/table/tr[%d]/td/table/tr" % (i))
                    # print(i)
                    for index in range(1, len(value_range)):
                        row = sheet.row(row_pount)
                        row.write(0, value_range[index].xpath("td[3]/text()").pop().strip())
                        row.write(1, value_range[index].xpath("td[4]/text()").pop().strip())
                        row_pount += 1

        row = sheet.row(row_pount)
        row.write(0, "-" * 50)
        row.write(1, "-" * 50)
        row_pount += 2
        me.pount["y"] = row_pount
        me.do_file.set(title, do_body)

    def get(me, *a, **k):
        if me.cache:
            url = os.path.join("data", "cache", hashlib.md5(a[0].encode('utf-8')).hexdigest() + ".html")
            if os.path.exists(url):
                with open(url, "rb") as f:
                    return f.read().decode()

        res = req.get(*a, **k)

        if res.status_code == 200 and me.cache:
            with open(url, "wb") as f:
                f.write(res.content)
        return res.text


class MemoryDoFile(object):
    """docstring for MemoryDoFile"""
    data = []
    year_current = 1

    def save(me, file):
        template = """#delimit ;



psid use \n"""
        with open(file, "wb") as f:
            text = template + "\n".join(me.data)
            f.write(text.encode())

    def set(me, title, body):
        # %02d
        if me._exist_list_variable(body):
            return

        title = title.lower().strip()
        title = ' '.join(title.split())
        title = title.replace("#", "_number")
        title = title.replace("?", "_question")
        title = title.replace(" ", "_")

        title = re.sub(r"[^\w_\s]", "", title, 0, re.IGNORECASE)

        regex = r"([0-9]{2,4})"
        matches = re.finditer(regex, title, re.IGNORECASE)
        years_finder = [int(match.group(1)) for matchNum, match in enumerate(matches)]
        years_finder.sort(reverse=True)
        for y in years_finder:
            year_current = int(me.year_current)
            if len(str(y)) == 2:
                year_current = int(str(me.year_current)[2:])
            if y == year_current:
                title = title.replace(str(y), "_current_year")
            elif y == (year_current - 1):
                title = title.replace(str(y), "_last_year")
            elif y == (year_current - 2):
                title = title.replace(str(y), "_two_years_ago")
        title = title.replace("__", "_")
        if title[0] == "_":
            title = title[1:]

        sep = 1
        title_sep = title
        while True:
            if me._exist_title(title_sep):
                sep += 1
                title_sep = "%s_%02d" % (title, sep)
            else:
                break
        title = title_sep
        if title:
            me.data.append("\t|| %s" % title)
        if body.strip():
            me.data.append("\t%s" % body.strip())

    def _exist_title(me, title):
        for txt in me.data:
            title = ("\t|| %s" % title)
            if "||" in txt and title == txt:
                return True
        return False

    def _exist_list_variable(me, value):
        search = ''.join(value.split())
        for txt in me.data:
            txt = "".join(txt.split())
            if "||" not in txt and search[:2] == txt[:2] and search == txt:
                return True
        return False


class MergeDoFile(object):
    fileoutput = "do_file_merge.do"

    def get_key_sort(me, filename):
        regex = r"([0-9]{2,4})"
        matches = re.finditer(regex, filename, re.IGNORECASE)
        for matchNum, match in enumerate(matches):
            return match.group(1)
        return None

    def merge(me, *files):
        array = []
        files = sorted(files, reverse=True, key=me.get_key_sort)
        for file in files:
            if os.path.exists(file):
                with open(file, "r") as f:
                    print("opening ", file)
                    for line in f.readlines():
                        if not line.strip():
                            continue
                        if not (('//' in line) or ('#' in line) or ('psid use' in line)):
                            array.append(line.replace("\n", ""))
            else:
                print("file does not exists", file)
        do_file_merge = MemoryDoFile()
        do_file_merge.year_current = -1000
        # do_file_merge.data = array
        print("merging...")
        for index, arg in enumerate(chunks(array, 2)):
            if len(arg) == 1:
                continue
            if "||" in arg[1]:
                continue
            arg[0] = arg[0].replace("||", "")
            # arg[0] = re.sub(r"(_[0-9]{1,3})", "", arg[0], 0, re.IGNORECASE)
            do_file_merge.set(*arg)
        do_file_merge.save(me.fileoutput)
        print("output in: %s" % me.fileoutput)

    def run(me):
        # path = input("input folder path:")
        me.fileoutput = "merge/output/psif_merge_all.do"
        me.merge(*glob.glob("merge/input/*.do"))


for path in [os.path.join("data", "cache"), os.path.join("data", "xls"), os.path.join("data", "do"), os.path.join("merge", "input"), os.path.join("merge", "output")]:
    if not os.path.exists(path):
        os.makedirs(path)

if __name__ == '__main__':
    s = Scraper()
    s.run()
    # m = MemoryDoFile()
    # m.year_current = 1995
    # m.set("1995 INTERVIEW 94# ? &%#", "[93]V21601 [94]ER2001")
    # m.set("1995 INTERVIEW 94#", "[93]V21601 [94]ER2001")
    # print(m.data)

    # tool = MergeDoFile()
    # tool.run()
    # text = "hehc_slf_rpt_par_sep_age_2hd"
    # text = text.replace("||", "")
    # text = re.sub(r"(_[0-9]{1,3})", "", text, 0, re.IGNORECASE)
    # print(text)
